#include "outs.h"
#include "stm32f1xx_hal.h"
#include "main.h"

void out_Y00_set(void){
    HAL_GPIO_WritePin(OUT_Y00_GPIO_Port,OUT_Y00_Pin,GPIO_PIN_SET);
}

void out_Y00_reset(void){
    HAL_GPIO_WritePin(OUT_Y00_GPIO_Port,OUT_Y00_Pin,GPIO_PIN_RESET);
}

void out_Y01_set(void){
    HAL_GPIO_WritePin(OUT_Y01_GPIO_Port,OUT_Y01_Pin,GPIO_PIN_SET);
}

void out_Y01_reset(void){
    HAL_GPIO_WritePin(OUT_Y01_GPIO_Port,OUT_Y01_Pin,GPIO_PIN_RESET);
}

void out_Y02_set(void){
    HAL_GPIO_WritePin(OUT_Y02_GPIO_Port,OUT_Y02_Pin,GPIO_PIN_SET);
}

void out_Y02_reset(void){
    HAL_GPIO_WritePin(OUT_Y02_GPIO_Port,OUT_Y02_Pin,GPIO_PIN_RESET);
}

void out_Y03_set(void){
    HAL_GPIO_WritePin(OUT_Y03_GPIO_Port,OUT_Y03_Pin,GPIO_PIN_SET);
}

void out_Y03_reset(void){
    HAL_GPIO_WritePin(OUT_Y03_GPIO_Port,OUT_Y03_Pin,GPIO_PIN_RESET);
}

void out_Y04_set(void){
    HAL_GPIO_WritePin(OUT_Y04_GPIO_Port,OUT_Y04_Pin,GPIO_PIN_SET);
}

void out_Y04_reset(void){
    HAL_GPIO_WritePin(OUT_Y04_GPIO_Port,OUT_Y04_Pin,GPIO_PIN_RESET);
}

void out_Y05_set(void){
    HAL_GPIO_WritePin(OUT_Y05_GPIO_Port,OUT_Y05_Pin,GPIO_PIN_SET);
}

void out_Y05_reset(void){
    HAL_GPIO_WritePin(OUT_Y05_GPIO_Port,OUT_Y05_Pin,GPIO_PIN_RESET);
}

void out_Y06_set(void){
    HAL_GPIO_WritePin(OUT_Y06_GPIO_Port,OUT_Y06_Pin,GPIO_PIN_SET);
}

void out_Y06_reset(void){
    HAL_GPIO_WritePin(OUT_Y06_GPIO_Port,OUT_Y06_Pin,GPIO_PIN_RESET);
}

void out_Y07_set(void){
    HAL_GPIO_WritePin(OUT_Y07_GPIO_Port,OUT_Y07_Pin,GPIO_PIN_SET);
}

void out_Y07_reset(void){
    HAL_GPIO_WritePin(OUT_Y07_GPIO_Port,OUT_Y07_Pin,GPIO_PIN_RESET);
}

void out_Y10_set(void){
    HAL_GPIO_WritePin(OUT_Y10_GPIO_Port,OUT_Y10_Pin,GPIO_PIN_SET);
}

void out_Y10_reset(void){
    HAL_GPIO_WritePin(OUT_Y10_GPIO_Port,OUT_Y10_Pin,GPIO_PIN_RESET);
}

void out_Y11_set(void){
    HAL_GPIO_WritePin(OUT_Y11_GPIO_Port,OUT_Y11_Pin,GPIO_PIN_SET);
}

void out_Y11_reset(void){
    HAL_GPIO_WritePin(OUT_Y11_GPIO_Port,OUT_Y11_Pin,GPIO_PIN_RESET);
}

void out_Y12_set(void){
    HAL_GPIO_WritePin(OUT_Y12_GPIO_Port,OUT_Y12_Pin,GPIO_PIN_SET);
}

void out_Y12_reset(void){
    HAL_GPIO_WritePin(OUT_Y12_GPIO_Port,OUT_Y12_Pin,GPIO_PIN_RESET);
}

void out_Y13_set(void){
    HAL_GPIO_WritePin(OUT_Y13_GPIO_Port,OUT_Y13_Pin,GPIO_PIN_SET);
}

void out_Y13_reset(void){
    HAL_GPIO_WritePin(OUT_Y13_GPIO_Port,OUT_Y13_Pin,GPIO_PIN_RESET);
}

void out_Y14_set(void){
    HAL_GPIO_WritePin(OUT_Y14_GPIO_Port,OUT_Y14_Pin,GPIO_PIN_SET);
}

void out_Y14_reset(void){
    HAL_GPIO_WritePin(OUT_Y14_GPIO_Port,OUT_Y14_Pin,GPIO_PIN_RESET);
}

void out_Y15_set(void){
    HAL_GPIO_WritePin(OUT_Y15_GPIO_Port,OUT_Y15_Pin,GPIO_PIN_SET);
}

void out_Y15_reset(void){
    HAL_GPIO_WritePin(OUT_Y15_GPIO_Port,OUT_Y15_Pin,GPIO_PIN_RESET);
}

void out_Y16_set(void){
    HAL_GPIO_WritePin(OUT_Y16_GPIO_Port,OUT_Y16_Pin,GPIO_PIN_SET);
}

void out_Y16_reset(void){
    HAL_GPIO_WritePin(OUT_Y16_GPIO_Port,OUT_Y16_Pin,GPIO_PIN_RESET);
}

void out_Y17_set(void){
    HAL_GPIO_WritePin(OUT_Y17_GPIO_Port,OUT_Y17_Pin,GPIO_PIN_SET);
}

void out_Y17_reset(void){
    HAL_GPIO_WritePin(OUT_Y17_GPIO_Port,OUT_Y17_Pin,GPIO_PIN_RESET);
}