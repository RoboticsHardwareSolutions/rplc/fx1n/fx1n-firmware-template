#include "inputs.h"
#include "main.h"

bool input_X00(void){
    return HAL_GPIO_ReadPin(INPUT_X00_GPIO_Port,INPUT_X00_Pin) == GPIO_PIN_RESET;
}

bool input_X01(void){
    return HAL_GPIO_ReadPin(INPUT_X01_GPIO_Port,INPUT_X01_Pin) == GPIO_PIN_RESET;
}

bool input_X02(void){
    return HAL_GPIO_ReadPin(INPUT_X02_GPIO_Port,INPUT_X02_Pin) == GPIO_PIN_RESET;
}

bool input_X03(void){
    return HAL_GPIO_ReadPin(INPUT_X03_GPIO_Port,INPUT_X03_Pin) == GPIO_PIN_RESET;
}

bool input_X04(void){
    return HAL_GPIO_ReadPin(INPUT_X04_GPIO_Port,INPUT_X04_Pin) == GPIO_PIN_RESET;
}

bool input_X05(void){
    return HAL_GPIO_ReadPin(INPUT_X05_GPIO_Port,INPUT_X05_Pin) == GPIO_PIN_RESET;
}

bool input_X06(void){
    return HAL_GPIO_ReadPin(INPUT_X06_GPIO_Port,INPUT_X06_Pin) == GPIO_PIN_RESET;
}

bool input_X07(void){
    return HAL_GPIO_ReadPin(INPUT_X07_GPIO_Port,INPUT_X07_Pin) == GPIO_PIN_RESET;
}

bool input_X10(void){
    return HAL_GPIO_ReadPin(INPUT_X10_GPIO_Port,INPUT_X10_Pin) == GPIO_PIN_RESET;
}

bool input_X11(void){
    return HAL_GPIO_ReadPin(INPUT_X11_GPIO_Port,INPUT_X11_Pin) == GPIO_PIN_RESET;
}

bool input_X12(void){
    return HAL_GPIO_ReadPin(INPUT_X12_GPIO_Port,INPUT_X12_Pin) == GPIO_PIN_RESET;
}

bool input_X13(void){
    return HAL_GPIO_ReadPin(INPUT_X13_GPIO_Port,INPUT_X13_Pin) == GPIO_PIN_RESET;
}

bool input_X14(void){
    return HAL_GPIO_ReadPin(INPUT_X14_GPIO_Port,INPUT_X14_Pin) == GPIO_PIN_RESET;
}

bool input_X15(void){
    return HAL_GPIO_ReadPin(INPUT_X15_GPIO_Port,INPUT_X15_Pin) == GPIO_PIN_RESET;
}

bool input_X16(void){
    return HAL_GPIO_ReadPin(INPUT_X16_GPIO_Port,INPUT_X16_Pin) == GPIO_PIN_RESET;
}

bool input_X17(void){
    return HAL_GPIO_ReadPin(INPUT_X17_GPIO_Port,INPUT_X17_Pin) == GPIO_PIN_RESET;
}