#ifndef __INPUTS_H_
#define __INPUTS_H

#include "stm32f1xx_hal.h"
#include "stdbool.h"

bool input_X00(void);

bool input_X01(void);

bool input_X02(void);

bool input_X03(void);

bool input_X04(void);

bool input_X05(void);

bool input_X06(void);

bool input_X07(void);

bool input_X10(void);

bool input_X11(void);

bool input_X12(void);

bool input_X13(void);

bool input_X14(void);

bool input_X15(void);

bool input_X16(void);

bool input_X17(void);

#endif
