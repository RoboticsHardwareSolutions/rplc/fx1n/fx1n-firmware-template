#ifndef __LED_H_
#define __LED_H_

#include "stm32f1xx_hal.h"



void led_power_on(void);

void led_power_off(void);

void led_error_on(void);

void led_error_off(void);

void led_run_on(void);

void led_run_off(void);

#endif
