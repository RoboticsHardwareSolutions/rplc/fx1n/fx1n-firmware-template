#include "led.h"
#include "main.h"


void led_power_off(void){
    HAL_GPIO_WritePin(LED_POWER_GPIO_Port,LED_POWER_Pin,GPIO_PIN_SET);
}
void led_power_on(void){
    HAL_GPIO_WritePin(LED_POWER_GPIO_Port,LED_POWER_Pin,GPIO_PIN_RESET);
}

void led_error_off(void){
    HAL_GPIO_WritePin(LED_RUN_GPIO_Port,LED_ERROR_Pin,GPIO_PIN_SET);
}
void led_error_on(void){
    HAL_GPIO_WritePin(LED_RUN_GPIO_Port,LED_ERROR_Pin,GPIO_PIN_RESET);
}

void led_run_off(void){
    HAL_GPIO_WritePin(LED_RUN_GPIO_Port,LED_RUN_Pin,GPIO_PIN_SET);
}
void led_run_on(void){
    HAL_GPIO_WritePin(LED_RUN_GPIO_Port,LED_RUN_Pin,GPIO_PIN_RESET);
}
