/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define RUN_SWITCH_Pin GPIO_PIN_2
#define RUN_SWITCH_GPIO_Port GPIOE
#define RS485_DD_Pin GPIO_PIN_1
#define RS485_DD_GPIO_Port GPIOA
#define RS485_UART_TX_Pin GPIO_PIN_2
#define RS485_UART_TX_GPIO_Port GPIOA
#define RS485_UART_RX_Pin GPIO_PIN_3
#define RS485_UART_RX_GPIO_Port GPIOA
#define INPUT_X07_Pin GPIO_PIN_0
#define INPUT_X07_GPIO_Port GPIOB
#define INPUT_X06_Pin GPIO_PIN_1
#define INPUT_X06_GPIO_Port GPIOB
#define INPUT_X05_Pin GPIO_PIN_2
#define INPUT_X05_GPIO_Port GPIOB
#define INPUT_X17_Pin GPIO_PIN_10
#define INPUT_X17_GPIO_Port GPIOE
#define INPUT_X16_Pin GPIO_PIN_11
#define INPUT_X16_GPIO_Port GPIOE
#define INPUT_X15_Pin GPIO_PIN_12
#define INPUT_X15_GPIO_Port GPIOE
#define INPUT_X14_Pin GPIO_PIN_13
#define INPUT_X14_GPIO_Port GPIOE
#define INPUT_X13_Pin GPIO_PIN_14
#define INPUT_X13_GPIO_Port GPIOE
#define INPUT_X12_Pin GPIO_PIN_15
#define INPUT_X12_GPIO_Port GPIOE
#define INPUT_X04_Pin GPIO_PIN_12
#define INPUT_X04_GPIO_Port GPIOB
#define INPUT_X03_Pin GPIO_PIN_13
#define INPUT_X03_GPIO_Port GPIOB
#define INPUT_X02_Pin GPIO_PIN_14
#define INPUT_X02_GPIO_Port GPIOB
#define INPUT_X01_Pin GPIO_PIN_15
#define INPUT_X01_GPIO_Port GPIOB
#define INPUT_X10_Pin GPIO_PIN_8
#define INPUT_X10_GPIO_Port GPIOD
#define INPUT_X11_Pin GPIO_PIN_9
#define INPUT_X11_GPIO_Port GPIOD
#define LED_POWER_Pin GPIO_PIN_13
#define LED_POWER_GPIO_Port GPIOD
#define LED_RUN_Pin GPIO_PIN_14
#define LED_RUN_GPIO_Port GPIOD
#define LED_ERROR_Pin GPIO_PIN_15
#define LED_ERROR_GPIO_Port GPIOD
#define INPUT_X00_Pin GPIO_PIN_6
#define INPUT_X00_GPIO_Port GPIOC
#define OUT_Y00_Pin GPIO_PIN_7
#define OUT_Y00_GPIO_Port GPIOC
#define OUT_Y01_Pin GPIO_PIN_8
#define OUT_Y01_GPIO_Port GPIOC
#define OUT_Y02_Pin GPIO_PIN_9
#define OUT_Y02_GPIO_Port GPIOC
#define OUT_Y03_Pin GPIO_PIN_8
#define OUT_Y03_GPIO_Port GPIOA
#define OUT_Y07_Pin GPIO_PIN_9
#define OUT_Y07_GPIO_Port GPIOA
#define OUT_Y06_Pin GPIO_PIN_10
#define OUT_Y06_GPIO_Port GPIOA
#define OUT_Y05_Pin GPIO_PIN_11
#define OUT_Y05_GPIO_Port GPIOA
#define OUT_Y04_Pin GPIO_PIN_12
#define OUT_Y04_GPIO_Port GPIOA
#define OUT_Y17_Pin GPIO_PIN_0
#define OUT_Y17_GPIO_Port GPIOD
#define OUT_Y16_Pin GPIO_PIN_1
#define OUT_Y16_GPIO_Port GPIOD
#define OUT_Y15_Pin GPIO_PIN_2
#define OUT_Y15_GPIO_Port GPIOD
#define OUT_Y14_Pin GPIO_PIN_3
#define OUT_Y14_GPIO_Port GPIOD
#define OUT_Y13_Pin GPIO_PIN_4
#define OUT_Y13_GPIO_Port GPIOD
#define OUT_Y12_Pin GPIO_PIN_5
#define OUT_Y12_GPIO_Port GPIOD
#define OUT_Y11_Pin GPIO_PIN_6
#define OUT_Y11_GPIO_Port GPIOD
#define OUT_Y10_Pin GPIO_PIN_7
#define OUT_Y10_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
